## Chapter 1: Basic Rules
A band of heroic knights on a quest to bring justice to an ancient land, a team of interstellar explorers seeking new worlds and people to exploit, or a covert team of operatives hired by a secret government agency to troubleshoot its problems. These stories and many more are possible using the ShadowCatcher system. At its heart ShadowCatcher is a generic and universal system which focuses on story progression rather than complex rules.

While ShadowCatcher is intended to be rules light, it still allows for a depth of gameplay options and the chance for meaningful character development. The system aims to keep rules as consistent as possible and not suffer from the rules bloat that often occurs when additional elements (like magic and vehicles) come into play. At the core of the ShadowCatcher system are challenges and test rolls, which primarily occur due to requests by the players to complete tasks or solve problems.

A critical concept of the ShadowCatcher system is **the rule of threes**, whereby elements are broken down into three concepts, or numerical factors of three. This section covers the first of these, the interplay between characters, tests and challenges.

### Characters
In a ShadowCatcher game, each player controls at least one character. This is the player's interface or connection to the game being played (or story being told, depending on how you like to view the experience). A character should be much more than just a collection of numbers and dice to be rolled. They should have a core concept, beliefs and motivations to keep them engaged with the story.

Characters in ShadowCatcher have Traits which describe what they are good at, Relationships that show people and organisations that matter to them, special Possessions that mean something to them, and even areas they are not good at that they wish to change about themselves.

These Traits form a major part of the mechanics of the ShadowCatcher system, and are coupled with the approaches that the character's favour to get things done.

### Tests
Often in a ShadowCatcher game a player first describes their character's actions, or state what they want their character to do. The GM will then determine if the action can simply take place, or if a test might be needed to determine success. A test uses the total from a roll of one of your character’s Ability dice and a d20 to determine the final result of a proposed action.
#### Approaches
As ShadowCatcher works on an approach based system, it is useful to think of your character's actions based on how they would approach the problem. For example, if your character is presented with a locked door, they may choose to use brute strength and Forcefully ram the door, they may be more subtle and Cunningly trip the lock, or use their mental prowess and Cleverly find a flaw in the doors mechanism that allows it to be opened. Any of these approaches could feasibly overcome the obstacle, but the approach used by the character will determine the dice used, and therefore change the chance of success. For more information on these, see the Ability Score section later.

A test determines if your character’s raw talent and training are enough to overcome the obstacle presented to them. In many instances the GM will determine that a test is not needed, and that you will simply succeed. For example, if you are walking across a wide bridge in dry, calm conditions then the GM will be unlikely to ask you to roll to see if you can keep your footing on the bridge. However, if the GM determines that an action has a risk of failure with a significant consequence, he may call for a test. In contrast to the above example, an Agility test would be called for if the bridge were a narrow rope bridge, swinging in the lashing wind and rain of a storm. Here the dice aid in determining the outcome of your action.

If a test is required, your GM will create a challenge for you to overcome and allow you to roll one or more Ability tests to defeat it using any approach that they agree you would be reasonable able to take.
#### Performing a Test
To make a test you first need to decide on the approach you are going to use. As in the example of the locked door above, there may be more than one way to approach the problem, and it is wise to use the best approach you can that fits the problem. Not all approaches will reasonably fit with all challenges and the GM is the final arbiter on if an approach can be tested in this way.

Once you have agreed an approach, you should refer to the Ability on your character sheet.

_Roll the Dice_: To make a test, roll 1d20 and the dice listed next to your Ability (either 1d4, 1d6, 1d8, 1d10 or 1d12). The numbers listed on these two dice should then be added together.  
Announce the Total: Tell the GM the result of your test. Based on the level of the challenge faced, the GM will then tell you the outcome of your test.

_Using Traits, Relationships, Possessions and Progressions_: It may be that you have a positive Trait or Relationship relevant to the test you can call, or the GM may call a Progression Trait to work against you. In these cases the GM will reduce the challenge level by one step when you call a Trait (two steps if it is an improved Trait), or increase it by one step when attempting to use a Progression. As a player, you can also suggest using a Progression yourself if you wish. Traits and Progressions are fully described later in the Characters chapter.

A Possession can be used either on its own or to aid in a Test reducing the challenge level by one (or one additional step if a Trait has already been called). Only one Possession can be used to aid in a Test in this way.

**NB** only one Trait, Relationship or Progression can apply at a time from a single character, therefore they cannot be stacked (i.e. you cannot use two Traits to reduce a challenge level by two). However, if multiple characters are facing the same challenge, then they can reduce the challenge level further (see Aiding). Possessions are the exception to this as they can be used alongside a Trait, but only one possession can be used per test and other possessions cannot be used by aiding characters.

_Failure_: if you fail a test, it may be that you do not make progress or make progress with a twist, but either way you must choose one of your Abilities to become hampered, or alternatively to hamper one of your Traits (player choice).
#### Hampered Abilities and Traits
If an Ability has become hampered then you can still use it, but the challenge being faced is treated as if it were one level higher.

Traits that have become hampered cannot be called at all until they are recovered.
#### Destiny Points
In a ShadowCatcher game you characters are heroes, well above the ordinary and in possession of an important destiny or purpose. As such characters posses Destiny Points to represent this special nature. Characters start with a pool of 5 destiny points. These can be spent in one of two ways as follows:

* To reroll an Ability test
* To convert Progressions into Traits between stories (see later)

The use of destiny points for development will be discussed later, but the simplest use for them is to allow a player to reroll an Ability test. The result of this reroll must be taken even if it is worse than the original roll.

Destiny points are usually given out by GMs when players complete a story, but they can also be earned by accepting **called** Progressions.

If a GM calls one of the characters Progressions to disadvantage an Ability test, the player can chose one of two options. He can either accept the Progression and earn a destiny point, or spend a destiny point and ignore the Progression.

In this way GMs cannot force you to face a Progression, but neither can they be ignored lightly.
### Challenges
A challenge is the term used in ShadowCatcher to quantify an obstacle that must be overcome using at least one (but often more) Ability test. These come in two basic formats; the static challenge and the dynamic challenge.
#### Static Challenges
The static challenge is perhaps the simplest of all challenges in ShadowCatcher. This represents an obstacle which remains mostly passive to the presence of the characters, but which still requires the character to undertake some risk in order to tackle the challenge presented.

Examples of static challenges could include locked doors and chests, a scalable cliff, or a puzzle to solve. The key concept is that the static challenge shouldn't pose a threat to a character if that character chooses to not interact with it. When the GM creates a static challenge, they will give it at least one Trait that must be overcome in order to progress past the challenge.

To aid in describing how static challenges work in ShadowCatcher, we have divided them into simple and complex challenges. These essentially work in the same way, but have subtle differences due to the number of Traits the challenges contain.

_Simple Static Challenges_: A static challenge with a single Trait is referred to as a Simple Static Challenge, as this is the simplest challenge a GM can present to a character (i.e. a challenge whose outcome is determined by a single Ability test).

An example of a simple static challenge brings us back to the locked door obstacle. Here the GM might create the challenge of a **"A sturdy door"**, with the Trait **"A good lock"**. In this challenge the characters only have to bypass the lock in order to progress to story.

_Resolving a Simple Static Challenge_: GMs and players are encouraged to resolve the challenge after just one test roll. That is accepting that the attempt succeeded, failed or succeeded with a twist. If any player wishes to try to achieve the same goal again, and the GM feels it is appropriate, they must do so using another approach since the one used previously failed.

More advice on framing challenges and resolving them is covered in the Game Mastering section later.

_Complex Static Challenges_: As might seem logical, if a challenge with a single Trait is simple, then any with two or more Traits become complex challenges. These Traits might not all be named, but should be directly relevant to the challenge being faced.

To continue the example above, if the door not only had **"A good lock"** as stated before, but now also had a **"Swinging pendulum blade"** just before it, both these Traits would have to be overcome to complete the challenge successfully and get through the sturdy door. Adding extra Traits not only increases the number of Ability tests required to progress past the challenge, but also adds complexity as to how to resolve the challenge.

_Resolving a Complex Static Challenge_: As with any challenge a successful Ability test will overcome one of the Traits for the challenge, and once all the Traits have been overcome the challenge is complete. 

However, what happens when an Ability test is failed? At the very least, failing any Ability test will result in the character hampering either an Ability or a Trait. The onus is then on the GM to decide whether the test simply failed to overcome the Trait, or succeeded with a twist.

Which of these two resolutions is chosen is a matter for the GM and players to weigh up based on what fits best with the nature of the challenge, the pace of the scene and essentially what would make a better story. Often a success with a twist creates a new Trait for the challenge, or even sets up a new challenge to be faced either immediately, or in the near future. 

For more detailed advice on framing and resolving challenges see the Game Mastering section later.
#### Dynamic Challenges
Dynamic challenges are, to coin a phrase, challenges that challenge you back. Unlike a static obstacle these challenges in some way actively threaten the characters. The characters can therefore be defeated by the challenge whether they choose to interact with it or not. In these cases the characters not only have to make rolls to overcome the Traits of a challenge, they must also roll to resist Traits activated against them.

_When to have a Dynamic Challenge_: Generally dynamic challenges arise, as stated above, when the obstacle presenting the challenge would reasonably play an active role in somehow threatening the players during a challenge. The most obvious example of this is combat (which we look at in more detail later), but could also include situations where a player character and a non-player character are trying to achieve the same goal (such as in a race or contest), or where the obstacle is a threat by its very nature (such as a storm at sea for example). Again it is up to the GM to determine when a challenge is simple or dynamic based on what seems reasonable given the situation at the time.

_Dynamic Challenges and Turns_: During a dynamic challenge the action is broken down into arbitrary segments of time know as turns. The player characters in ShadowCatcher always act first and therefore do so at the beginning of each turn, deciding among themselves in what order they perform their actions if the sequence is important. Once this has been decided they make any necessary tests to attempt to remove Traits from the source(s) of the dynamic challenge. Once all the players have completed their turn the GM will check any remaining dynamic challenges to determine if they can target a character. If so the GM will use an unhindered dynamic Traits and force at least one player to make a test to resist them.

The resisting player chooses the approach, and therefore Ability, they wish to use to resist the challenge and can use a Trait to aid them if the GM agrees it is relevant. If they succeed in the test then they suffer no ill effects. Otherwise, they must choose either a Trait or an Ability to hinder and thus soak the damage from the dynamic challenge.

The GM is the final arbiter on which Traits can be used to soak damage in this way, but common sense should prevail. For example if two soldiers were fighting on a battlement the player failed his roll to resist an attack from his opponent, he could chose to have his “Good with a shield” Trait hampered even though he used his “Good with a sword” Trait during the fight, describing this as an injury to his shield arm as he blocked the blow from his opponent.

What he could not do, is choose his “Has a good relationship with Lady Somerville” Trait to be hampered unless he gave a good reason for why this should be so (e.g. if he explained that she was particularly vain about her associates and he had a new scar that made his appearance unacceptable to her - which would be a bit of hard case to argue). GMs are encouraged to reward creativity, but should take into consideration what seems reasonable and fair given the setting and style of game being played.

**NB** If a player succeeds in their resist test then their reward is not having to hamper a Trait or Ability, they do not get to overcome any of the challenge Traits during this test.

_Resolving a Dynamic Challenge_: The resolution of a dynamic challenge is usually based on whether the players overcame all the Traits of the challenge, if they chose to concede, or if they were taken out by the challenge. The GM is the final arbiter in what this means to the players, and what their next challenge will therefore be.

### Combat
If the goal of a challenge is to overpower an opponent(s) or otherwise defeat them, then this should be resolved as a dynamic challenge. In this way the rules for combat are really no different to any dynamic challenge, except for the one rule below.

At the end of each turn participants in the combat can choose to continue the challenge, or concede and allow the other side to win. If at least one player character chooses not to concede the sombat, then the contest continues until either all the remaining players characters have been taken out of the scene (see later), or they overcome the challenge. 

It is not necessary for all the characters involved to agree to concede at the same time, and thus those who are suffering badly can concede and leave those better able to continue the fight. Once a character has conceded, however, they cannot rejoin the combat on a later turn.

If all the players choose to concede in combat, then this is often  essentially them retreating from the battle and the GM will usually take this into consideration when determining the outcome.

> #### Optional Rule: Surprise
> This is an optional rule which can be used where the players and GM feel that it is important to know whether one side in a combat situation may have surprised the other. If the GM feels that one or either side could be surprised in a combat challenge, then he may ask the players to make a Perception test based on the Challenge Level (to see if they are surprised) and/or ask them to test their Cunning or Agile Ability to see if they surprise their opponents. If one party if surprised, then the other can have a free round of tests before the other can act.
### Multiple participants and Aiding
If there are multiple participants in a test, usually more than one player trying to achieve the same aim, then characters may have the ability to aid each other in completing the test. In such cases one character should be identified as the lead character, while the other characters assisting must lend Traits to aid the lead in completing the test. 

To aid another in a test the augmenting characters must describe what they are doing, and how this will aid the lead's action. In order to do this they need to specify an appropriate Trait they can use which is different to that being used by the lead character. As always GMs are encourage to use their discretion as to whether the proposed action is in keeping with the rules of the game.

The usefulness of having such aid is that for every character that assists the lead, the difficulty level of the challenge is reduced by 1 up to a maximum reduction of 3 levels of difficulty. The rules assume that once more than four people are involved in a task, the complexity of co-ordinating a large team outweighs the benefit of the teamwork. Exceptions to this may exist, but the GM is the final arbiter in exactly how many characters can aid with a task. The downside to offering aid is that if the test fails then **all** the Traits involved from **all** the characters become hampered.

**NB** To aid a character you need to have an unhampered Trait as listed above, you cannot aid with just an Ability as the aiding character should demonstrate an above average competence at something in order to give aid.
### Burning a Trait, Possession or Relationship
There may come a time in your hero's adventuring that the risk of failure is just not acceptable and you want to be assured of success. Maybe you are facing your arch nemesis, or trying to save the one you love. Either way a mechanism exists to allow you an automatic success on a test, but doing so comes at a cost.

At any time a player can choose to 'burn' a Trait, Possession or Relationship to gain an automatic success. When you chose to burn such a Trait, Possession or Relationship in this way it is permanently lost to your character and cannot be recovered as described in chapter 4. This should not be done lightly and it is recommended that this is mainly reserved for Possessions and only for Relationships and Traits if the need is great or the cause dramatic enough to justify it.

How the burned Trait, Possession or Relationship is actually 'destroyed' in the game is best agreed between the player and the GM. This is simple for items which can literally be destroyed, but with relationships it could involve a death or a ruined relationship. Traits that are burned would suggest some kind of permanent injury, and as previously stated, ought to be reserved for extreme cases.