## Introduction
The ShadowCatcher system is intended to be a rules light, generic, modular system with a substantial focus on promoting a narrative style over complex rules. These rules are still in the beta stage, and could change substantially based on feedback.

As with all roleplaying games, ShadowCatcher games take place in your mind, using imagination over boards or computer generated characters. The Games Master (GM) describes scenes and the actions of other people within the world to you and your fellow players. Using your imagination you respond by asking questions and describing the actions of your characters in turn. If a contest arises, then your character's abilities are used to overcome the challenges presented.
>"ShadowCatcher games take place in your mind" 

These rules assume that players have access to a least one set of polyhedral dice (1d4, 1d6, 1d8, 1d10, 1d12 and 1d20), a character sheet (though a blank piece of paper can be used), and a pen, pencil or similar implement. If you are the GM you should have a copy of the rules, a way to take notes, and either a prewritten adventure, or one of your own making. Some GMs and players like to use miniatures, battle mats, dungeon tiles, scenery and other such visual aids to enhance the experience, and although these can be used with the game, they are not required to play.
