## Chapter 6: Managing Challenges
The art of managing how and when you use challenges may be the skill that will require the most time and effort to master. As such, quite a substantial portion of this section of the rules has been dedicated to any tips and expansion on how to run challenges. At its simplest form a challenge should occur during a scene, if and when at least one player proposes an action that you as the GM feel has a risk of going wrong in a dramatically appropriate way, but let us break the process down into some steps that as the GM you should consider.
#### Is a Challenge required?
This is an important question that should always be asked. Really think about what making the player roll for and will the result of this add to the scene and their enjoyment of the game.

Many a game has been spoiled by characters having to roll to go up and down a ladder, in a well-lit house, in perfect conditions. Other than the comedy factor if it goes wrong (which may be appropriate depending on the kind of game you are playing), what does that add?

Now, climbing up a ladder made slippery due torrential rain, as you attempt to flee a band of thugs bent on your destruction... that makes for some drama. Here, a challenge would be perfect.

As the GM it is your job to keep the game flowing, so remember that challenges can break up the flow of a story if used too often, but can really add to the tension when used at the right time.

If a test does feel appropriate, then next step is to determine what is occurring, and what the player wants to achieve. In other words, we need to frame the challenge.
#### Framing the Challenge
Once the decision has been made that a challenge has been encountered the next step is to frame the challenge. That is to write down some brief description of what the challenge is, what the challenge level should be, and what traits it has.

The first step can be very simple. If the players have encountered a steep cliff face, then the challenge can be 'Cliff face'. Likewise it can be as fundamental as a 'Locked door', 'Secure computer network', or whatever else you feel is appropriate. It's not worth agonising over how narrow or broad a challenge definition is, just go with whatever feels right. You could have a challenge that is 'Break into Castle Krag' with multiple traits around the locked gate, guards on duty, patrols on the walls etc. or you could frame each of these as their own challenge with their own challenge level. The best way to judge it is to look at pace.

If you want the challenge to resolve quickly (somewhat like a montage from a movie), then a broad challenge with multiple traits works better. If however, you want to draw out the experience, perhaps creating more tension with punctuated scenes between them, then go ahead and break it down into multiple challenges. 

In the end, it will only be through experimenting with these methods that you and your player group will find the balance that best.
#### Setting the Challenge Level
Next you will have to determine the challenge level. In ShadowCatcher challenge levels range from 1 to 10, with 1 being the easiest, and 10 being the hardest challenge that could be realistically overcome. 

As a rule of thumb, level one challenges are very simple hardly needing a test at all, level 5 challenges should be considered literally 'challenging', and level 10 challenges being near impossible (i.e. something that someone at the peak of their performance could only complete if they were very lucky). 

It is best to think of a challenge in terms of how hard it would be for an average person to complete it, and then assign a challenge level based on this. Similarly for NPCs, consider how much of a challenge they would pose to your average Joe and assign a level to them based on this.

Here is another area where it is best not to waste too much time over the exact level you set, but instead to experiment and get a sense of what feels like the right challenge levels for your players.

As we have already stated ShadowCatcher utilises the rule of threes. Therefore to determine the target number (TN) - the number a player has to roll equal to or above to successfully complete a test -  for a challenge you multiply the challenge level by 3. So a level one challenge has a TN of 3, a level 4 challenge a TN of 12 and so on. The table below summarises the challenge levels, associated TNs as well as a rough description of the difficulty level and suggested number of traits for complex challenges (see later).

| Challenge Level  | Difficulty  | Target Number | Traits|
| :---: | :---: | :---: | :---: |
| 1  | Simple |  3  | 1 |
| 2  |  | 6 | 1 to 2 |
| 3  | Demanding | 9 | 2 to 3 |
| 4  |  | 12 | 2 to 4 |
| 5  | Challenging | 15 | 3 to 5 |
| 6  |  | 18 | 3 to 6 |
| 7  |  | 21 | 4 to 7 |
| 8  | Heroic | 24 | 4 to 8 |
| 9  |  | 27 | 5 to 9 |
| 10  | Near Impossible | 30  | 5 to 10 |


#### Assign Traits
Every challenge should have at least one trait - otherwise it wouldn't be a challenge at all. Furthermore these traits should be relevant to the challenge in question. For example, a security door could have the traits of **'Security camera'**, **'Pressure Plate'** and **'Electronic lock'**, where as a door guard might have the traits **'Belligerent'** and **'Tough'**. When thinking of traits consider what elements of a challenge the players will have to systematically bypass in order to overcome that challenge. How the players get past those elements is up to them.

To determine the number of traits first consider if this is a complex or simple challenge. A simple challenge by nature only has one trait (the challenge itself) and so your job becomes very easy. If you feel that it should be a complex challenge then you next need to look at the level of the challenge.

As stated above there should be at least one, but also no less than half the challenge level (rounded up) and no more that the challenge level itself. For example, a level 2 challenge can have 1 to 2 traits, a level 4 challenge 2 to 4 traits, a level 7 challenge 4 to 7 traits. The suggested number of traits for complex challenges of all the levels are summarised in the table above for your convenience.

It should be noted that this rule can be bent or even broken, in the name of making a challenge longer or shorter. Just make sure that you consider that extra traits in a low level challenge just brings in needless dice rolling, while extra traits for higher challenge levels could make the challenge a very long slog to complete or even risk taking out the party altogether.
#### Resolving a Test
Once the challenge has been fleshed out, the players will start describing actions and making ability tests. The question then becomes what does the result of the roll mean?

As stated above the difficulty level of the challenge determines the TN the test has to meet or beat. If the player's roll does indeed equal or exceed this number then the test was successful and the trait on the challenge card should be ticked to signify that it has been overcome. If the challenge had only one trait then the challenge should be resolved (see below), otherwise another player, or the same player with their turn next, can move on to the next trait.

If the roll is less than the target number however, then the player must chose a trait to hamper. This trait then becomes unusable until it is recovered (see Recovery later). If a player has no more applicable traits left to hamper, then will have to choose an ability to hamper instead. Often the player or players will then have to try and overcome the trait in another way, though the GM has the option to allow them to overcome it anyway (effectively defeating it at the cost of the trait) if they feel this is appropriate (see Useful failures later).
#### Resolving the Challenge
A challenge end when either all the challenge's traits have been overcome, or all the players involved have been taken out or conceded the challenge (see Hampering, Injury and Death). It is at this point that the GM must decide what the outcome of the challenge was.

If the players were successful, the scene should move on with the players happy in the knowledge of a job well done. If they were taken out or conceded the challenge then the GM will have to think of an appropriate outcome.

In any case the outcome should reflect that they have suffered a setback somehow. Either they have been captured, reported to authorities, failed to save something (maybe even someone), failed to obtain something, or obtained it at some significant cost.

The key here is to keep the story progressing. A failed challenge should not end the game or the story, but instead should add some new complication to the lives of the player characters. In essence, the failure becomes useful for the story.
#### Useful failures
This is not a new concept to many games, and is sometimes referred to as failing forward, or succeeding with cost or some such similar term. In effect, it is a tool the GM has to allow the story to progress despite poor dice rolls. There is nothing worse in a game than the players not getting into a locked room with a significant magical item, just because the thief rolled poorly. Instead it is more satisfying to move the story on, with some cost added on.

If the challenge and therefore test isn't a significant part of the main story, then you can as the GM choose to hamper a player character's trait on a failure, but also allow them to overcome the challenge trait they were taking on. This provides a cost to the failure, but stops the problem of multiple players trying to pick the same lock, listen at the same door etc. Although this is the quickest and therefore simplest way to progress the game, it also worth noting that it can be the most boring way to do it. Due to this, my recommendation is to look at the other option when handling a failure.

This second option involves overcoming the trait or challenge but adding a complication. The exact form of the complication can vary depending on the scene and GM discretion. It could be a story complication, maybe the police or a powerful wizard are now after you, having found out about your meddling. It could be that you overcome a trait for the challenge, but another trait is created in the process (essentially trait swapping). Consider a player character hacking a computer system, maybe they bypassed a firewall, but now a counter hacking program has been activated. Or at the extreme end you could add a whole new challenge (maybe they did open a locked chest, but now the door to the room has locked shut and it is filling with water!)

The exact nature of these failures are up to the GM and should always be in the name of making a good story and advancing that story somewhere. The idea is not to make a challenge long a laborious, but to make the player interactions with it dynamic and interesting. In short, failures should often make the game more fun.
#### Reframing the Challenge
Let us consider that you have framed your challenge, you have determined the obstacles and given your challenge a challenge level and assigned traits to it. You're all set to go, and then one player states "You about if I do..?" and they suggest a way to totally bypass your challenge or solve it in a way that doesn't make sense with the traits you have assigned to it. If you are an experienced GM you will find this familiar. As the GM it is up to you how you resolve this, but the ShadowCatcher rules suggest that in these situations you reframe the challenge.

To give an example, let us look at an example such as 'Breaking into Fortress Krag'. Perhaps as the GM you decided that you wanted a complex challenge involving a **'locked door'** with a ***'pressure plated trap'** and a **'riddle'** to solve. However, one of your player characters has the trait **'Good with casting air magic'** and has proposed that he flies the party over the fortress walls. 

Well, you can choose to tear up your challenge card and shout at your outlandish thinking players... or you can reframe the challenge and still make them work for it.

In this example we should consider that the players' idea is a good one, but it still has some risk. Flying over a wall is hardly inconspicuous and you still have to land somewhere and deal with the consequences. Therefore we reframe the challenge from 'Breaking into Fortress Krag', to 'Flying into Fortress Krag'. This challenge now at least has the trait of **'Guards on the wall'**, and maybe some other things the players hadn't considered such as a magical alarm or barrier, maybe the pressure plate you envisaged is actually in the courtyard and they might land on it etc. Here the players can still use their ingenuity, but you can still give them something to challenge them. 

A key note here is not to punish players for good ideas. Don't reframe the challenge with more traits than before or at an increased difficulty level (unless they are doing something you deem to be truly insane or difficult), but instead reward them for looing to their character's strengths to succeed. In that sense you can even consider reducing the difficulty lever, or number of traits, but this is by no means essential. The fact they may have redefined the challenge to align with their character strengths might be reward enough indeed.
### Non-Player Characters (NPCs)
Non-player characters are characters that the players interact with during the course of a story. How developed a character is will depend on how much interaction the players will need to have with them, but it's up to your personal taste as a GM as to how much time you dedicate to this.  They will range from the mundane and very transitory, to complex and recurring characters.

As a rule of thumb, it's best not to spend too much time worrying about the life story of Jim the bartender (though a little background is always nice), whereas the recurring villain who has dogged you since the first days of your story could have an extensive history and a full field of motivations. However detailed the character, the key thing to consider is that NPCs in ShadowCatcher are not usually treated as characters, but as Challenges.
#### NPCs as Challenges
In many instances an NPC encounter will occur without the need to make any tests, and you can resolve it just by describing your interaction. As the GM you are encourage where possible to handle as many NPC interactions in this way as you can. Consider the roleplaying aspect of the RPG over the 'roll' aspect. That being said, there will also be many instances where players will need to challenge an NPC, or the NPC will create an obstacle or challenge for them. Here, as the name suggests, the best way to handle the NPC is as a challenge.

Using the guidelines above for framing the challenge, you should consider what challenge level and NPC ought to pose to the players. Some minor bit part player in your story should be low, around 3 or less. Whereas a major antagonist should be much higher depending on the experience of your players, at least level 7 if not higher. In this way NPCs shouldn't require special rules (except if they work with other NPCs) and you should treat them exactly the same as any other challenge, giving them a suitable number of traits.
#### Multiple NPCs
There will be instances where multiple NPCs will work together in order to create a harder challenge for you. In these instances there are two ways to handle this, either class them as two challenges, or as a simple rule for recalculating the challenge level increase the highest challenge level of the NPCs by 1 for each doubling of NPCs (i.e. by 1 for 2 NPCs, 2 for 4 NPCs etc.) up to a maximum increase by 3 for 8 NPCs working together. This is essentially the opposite mechanic from dropping the challenge level by having other characters aid you in a challenge.

When you make a successful test against a combined challenge, the player can chose which NPC and which of their traits they overcome. As a final note, if NPCs work together, any return dynamic challenges they make must also be in this combined way. That is the TN to resist their dynamic challenges is also at the higher challenge level.
#### Monsters
Monsters in ShadowCatcher should be handled as a challenge. Whether as a simple or dynamic challenge will depend on the situation. In many ways monsters are simply NPCs and as such you should frame them as a challenge as you would any other NPC encounter. Players may be able to work their way around them without combat, or even avoid them all together, they key thing is to remember the rule of threes and frame your monsters as a challenge.