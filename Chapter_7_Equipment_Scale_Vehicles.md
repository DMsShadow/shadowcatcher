# Chapter 7: Advanced Possessions, Scale and Vehicles
This chapter should be consider a collection of optional or advanced rules that can be used by you as the GM, but they are by no means required in order to fully enjoy the game. These rules look at another way to handle special possessions and large vehicles rather than the standard possessions rules.
## Advanced Possessions
The rules for possessions presented in chapter one are the simplest for running ShadowCatcher where items are useful but not a major part of a characters identity or story. If, however, you'd prefer items to have more detail, utility and significance then you may use the following optional rules.

In this variant each possession has its own record card which resembles the layout of a challenge (and as such an index card can be used to record the ownership and state of such possessions). On this card you note the possession’s name along with 1 to 3 Traits the possession has.

When using the possession to aid you during a test you call one of these Traits in the same way you would for your character. Thus, if you fail the test you can hamper a single Trait and not the possession as a whole.
For example, you could own a **‘Master Crafted Katana’** with the Traits of **‘Keen edge’** and **‘Perfect balance’**. If you used this weapon in a combat challenge and you failed a test, you could choose to hamper the **‘Keen edge’** trait and you would still be able to use the blade at a later time until you were forced to hamper the **‘Perfect balance’** Trait.

Recovery of possession Traits follows exactly the same rules as those for character Traits. Here we are considering if you are able to make repairs to the item, either partially or fully.
This optional rule will increase the longevity of any possessions and also give them a greater sense of ‘character’ in the game. These rules can also be used to give vehicles more utility in the games if you do not wish to use the basic rules or the advanced rules below for them.

## Scale
Before looking at advanced rules for vehicles and ships, there is a new attribute that is not covered by the standard rules which we must introduce, and that is Scale. This is an abstract measure of the size of the person, item, vehicle, monster or other aspect of the game that gives it a different level of influence or survivability during challenges.

Looking back at the core concept of ShadowCatcher, Scale fully utilises the rule of threes. Firstly in the sense that there are three levels of scale (going from 1 to 3), and secondly that the magnitude of the change due to scale moves up in steps of three. To explain exactly what that means we will look at the levels of scale in turn.

_Scale 1:_ This is the ‘base’ scale in a ShadowCatcher game and at its simplest form if the scale of a normal human (or similar creature or item). At this scale all Traits for the characters, vehicles or possessions have one check box per Trait, and if they succeed in a test they tick off one check box from a targeted Trait.

_Scale 2:_ The second point on the scale roughly covers creatures or possessions that are bigger than a person, but could still be used by one character (or two characters working together). These would include personal vehicles such as cars, bikes, and even a combat jeep or fighter aircraft. At this scale all Traits for the characters, vehicles or possessions have three check boxes per Trait, and if they succeed in a test they tick off three check boxes from a targeted character, possession or challenge.

This essentially means that at scale 2 you have 3 times as much stamina per Trait as something at scale 1, and do three times as much ‘damage’ to others than a scale one protagonist would.

_Scale 3:_ This scale is intended to represent the largest objects or creatures (they would be extremely huge creatures) in a ShadowCatcher game. Examples would include warships, spaceships, gargantuan creatures and the like. Possessions for these would need a large crew to control and maintain. At this scale all Traits for the characters, vehicles or possessions have nine check boxes per Trait, and if they succeed in a test they tick off nine check boxes from a targeted character, possession or challenge.

As you can imagine challenges or vehicles at such a large scale would make short work of and scale one character or vehicle. As such it is not normally recommended that you deal with challenges of such a disparity, but then again such a conflict could be the stuff of legends.

To summarise, the main impact of scale is in dealing and absorbing hindrances during a test against an opponent of a differing scale. To a target of a higher scale, it takes three times as many losses to produce a hindrance for each level of difference (e.g. 3 for scale 1 to 2, and 9 for scale 1 to 3). If an opponent of a higher scale is actively attacking a target of a lower scale it takes 3 times as many hindrances per loss for every level of difference in scale.

## Vehicles
The advanced vehicle rules here treat player operated vehicles as larger versions of characters (as opposed to challenges which opponent vehicles would be), in the sense that a vehicle record sheet is an extrapolation of a player character sheet.

Each vehicle has attributes (derived from the pilot or crew of the vehicle depending on its size and scale, see later), as well as traits based on features and equipment found in the vehicle. These should be in line with how powerful the GM wishes the vehicle to be.

This is deliberate in order to keep the mechanics of ShadowCatcher as consistent and simple as possible. Since the mechanics of vehicles follows that of characters, so do all the subsequent tests and results of challenges.
These rules are best used where actions involving the vehicle are in someway central or at least very important to the story unfolding. If you consider some examples in popular fiction, reducing the USS Enterprise or the Millennium Falcon to a single possession or a card with a few Traits would be doing them an injustice.

Conversely, in a standard fantasy setting, you wouldn’t want a whole vehicle sheet for the horse are cart you use to lug around your loot. Common sense should prevail as to whether you use these rules or not.
### Scale
The vehicles scale should be determined based on the relative size of the vehicle and in line with the rules for scale detailed above.

The scale of a vehicle is important as it will determine aspects of the vehicle such as the maximum number of crew a vehicle can have as well as the number of Traits it will possess (see Table 2).
### Abilities
The abilities of a vehicle are derived from those of the pilot or crew of that vehicle. If the vehicle has only the one crew slot (i.e. just a single pilot or driver), then all of the vehicle’s abilities are drawn from those of this crew member (see Crew Positions later).

If the vehicle has multiple crew slots, then the abilities of the characters manning those stations determines the abilities of the vehicle. Usually, the larger the vehicle, the more numerous the crew slots and therefore the better able they are to have competent abilities in multiple areas.

Like much of the ShadowCatcher rules how the abilities relate to crew stations is quite abstract, but consider the following as a rough guide.

Agile will often represent a helm or driving station; Clever a science, computer or leadership station; Cunning is often related to stealth system, or similar but could also relate to piloting; Forceful is more often than not a weapon or weapons station; and finally Perceptive is a lookout, observation or sensor post.

It is up to the you as the GM to work with your players in determining which Abilities link to which position in a vehicle, but if in doubt, err on the side of helping the players be more competent.

Abilities do not have to be distributed equally from all characters and a vehicle can use four abilities from one character and one from another if the players feel this is the best use of their skills.

This would represent on character manning more than one station, multitasking, cross linking controls or similar. Due to how characters assign abilities however, this will always lead to a drop in abilities and so utilising a good mix of characters is often better.

For scale two and three vehicles you as the GM can also allow players to utilise ‘generic’ crew to man stations. This represents drafting in those nameless extras to fill in when a main character cannot help. The downside here is that they have an average ability score of d6, which maybe not brilliant, but may be better than a d4.
### Traits
When using a vehicle in a challenge the vehicle’s Traits are not derived from the crew, but are unique to that vehicle. Table 2 shows how many traits a vehicle has for its designated scale, though as the GM you do have some discretion to later these if you feel it is necessary.

A vehicle’s Traits are based around the systems present in a vehicle and therefore will often involve standard systems such as propulsion, manoeuvring, shields, weapons and the like. The exact details of these are up to you as the GM in discussion with your players.
### Upgrade Traits
These Traits are similar to progression traits possessed by characters. They are essentially aspects of the vehicle which perform below average due to its size, or design etc. As such these can be called in the same way as progressions during a challenge.

As the name suggests they can be improved in the same way as progression traits, but the cost is generally higher. To ‘upgrade’ such a trait it must have been called and used as per the progression trait rules, and characters must spend a number of destiny points equal to the number of crew stations the vehicle has times two.

This means that the more crew a vehicle has the better its abilities tend to be, but the harder it is to upgrade. 
Another notable exception here is that once a Trait has been upgraded, you do not choose any new upgrade traits. Unlike characters there is a limit on how much you can upgrade a vehicle.
### Crew positions
Vehicles of a scale higher than one possess multiple crew positions that can be manned by player characters. What this means in practice is that multiple characters can assign their own abilities in the required slots for the vehicle.

Therefore, the more crew slots a vehicle has, the greater the chance a character can assign the highest ability (d12) to that vehicles ability slot.

As already stated the crew positions do not have to manned equally, though it is usually advantageous to do so depending on your characters abilities.

Table 2 shows the maximum number of crew stations a vehicle can have for each scale, but the vehicle can have less than these in the players and GM agree. This will make it easier to upgrade, but harder to get good abilities for the vehicle.

Again, common sense should to prevail. As much as a player might want to, it is unlikely a starship would have a crew of one, even if they may be able to command one solo at a pinch.

Likewise, it’s unlikely you could have a motorbike manned by three people (although exceptions might exist).
 
**Table 2: Vehicle Scale and Attributes**

| Scale | Max. Crew | Traits | Upgrades |
| :---: | :---: | :---: | :---: |
| 2 | 3 | 3 | 2 |
| 3	| 5 | 9 | 4 |

### Aiding another Vehicle
When multiple vehicles work together they use the aid rules as normal, using vehicle traits to aid each other. The rules regarding a failed test when aiding apply and both vehicles will have traits hampered.

As a further optional rule, if a character has a trait which they feel would aid them in their crew station (perhaps ‘Good at piloting a fighter jet’) then at they may be able to use this to ‘aid’ the vehicle at the GMs discretion. In this case the rule for maximum number of aiding traits applies (i.e. a maximum of three levels of difficulty reduction), and if the test fails the character and the vehicle will have to hamper traits as normal.
### Taking out a Vehicle
A vehicle is considered to be taken out of a scene once all its Traits and abilities have been hampered. When this occurs all the crew of the vehicle are also considered to have been taken out. Though what this actually means is open to agreement with your players.

While a vehicle may be out of action, it might be possible for characters to escape in some way and progress to the next scene. Where they may have to flee, escape the exploding ship, or some similar dramatic scene.
### Vehicle Recovery
If a vehicle has hampered traits or abilities (essentially damaged crew stations) then these are recovered using the same rules as stipulated for characters traits, abilities and possessions. In this case the crew are jury rigging repairs or even taking a ship in for long-term repairs if needed.
### Vehicle ‘Death’
Much like character death, players have the option of allowing a vehicle to be destroyed if it becomes totally hampered and taken out of a scene. Indeed this is encouraged more for vehicles compared to the death of a player character.

The destruction of a vehicle ought to be dramatically fitting and a good GM would look for a way to furnish the player(s) with a new vehicle as soon as possible, and to make the sacrifice worthwhile. 