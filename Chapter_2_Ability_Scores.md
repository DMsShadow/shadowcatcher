## Chapter 2: Ability Scores
All characters have five abilities: Agile, Clever, Cunning, Forceful, and Perceptive. These represent the various approaches that your character can take in resolving tests and challenges in ShadowCatcher. When a character is first created they will have a die type (d4, d6, d8, d10 or d12) attached to each ability. Your ability die broadly defines your natural talent and skill in each of the five approaches. The higher your die type, the better your character is with that approach. Typically, all non-player characters and creatures do not have die types assigned to approaches as they do not roll any dice.

The d6 is considered average for a human adult, with a d12 being the highest achievable for a normal person. Adventurers don't have any scores above the d12, but instead rely on traits to aid them during tests.

Each ability is listed below followed by an example of how it could be called to help overcome a challenge using an Ability test.
### Agile
This is a measure of how, nimble, dextrous and quick your character is. The Agile ability is best called to overcome challenges where using speed, manual dexterity and balance are relevant.
#### Tests
An Agility test may be called whenever you feel speed, manual dexterity or nimbleness can be a major factor in overcoming a challenge. Examples include, tumbling past enemies, and walking across a narrow ledge to name but a few. An Agility test may also be used to avoid the effects of a dynamic challenge such as diving for cover from an explosion or dragon's breath, and avoiding traps, as well as resisting dynamic challenges that may keep you off balance, e.g. Staying upright on a deck of a ship during a storm.
### Clever
The Clever ability measures a characters intellect, memory, learning, recall, quick wits and anything else the GM determines that the Human brain has dominance over. Clevernesscan be called where brain power or memory could be the deciding factor in a test.
#### Tests
The GM may suggest a Clever test to determine if you know an obscure fact, or recall a piece of important information, and may even allow a tests to overcome challenges involving solving riddles. The Clever ability can also be used to resist effects of dynamic challenges that include effects like mental domination, trickery, or some spells.
### Cunning
This is a measure of how devious, sneaky and underhanded a character can be. It includes the use of disguises, stealth, concealment and any general deception. Cunning is suggested where deception and evasion is the main method to achieve your aims.
#### Tests
Cunning may be tested when the character is challenged and can avoid the situation through deception or stealth. Examples include sneaking past guards, or convincing them that you really work in the building. The Cunning ability can also be used to resist dynamic challenges where you could make a foe lose track of you through concealment and evasion, or where you could perhaps even bluff your way out of the situation.
### Forceful
This measures your ability to exert physical force using your raw muscle power. Forcefulness is the obvious choice to overcome challenges when climbing, swimming, jumping, fighting, long distance running, and any other situation that would rely on brute force, strength or stamina.
#### Tests
A Forcefulness test may be called for whenever a feat of force or endurance is required to overcome a challenge. Examples include opening a jammed door, pushing over an opponent, arm wrestling another person and so on. Similarly, a Forcefulness test may be needed to resist the effects of a dynamic challenge, such as a severe gust of wind, to hold on to a cliff face for a prolonged period of time, as well as challenges relating to poison or disease, as well as any supernatural effects that attack the body or health of a character or any other effect the you or the GM feels would require Forcefulness to resist.
### Perceptive
Perceptiveness measures how observant and aware of their surroundings a character is. It is used whenever a character may be able to notice something, or pick up on minor details that may be useful to them. This ability covers the physical senses such as sight, hearing, taste and touch.
#### Tests
A Perceptiveness test may be called for to overcome a challenge involving a hidden trap, deciphering voices behind a door, or to notice bandits waiting in ambush behind dense foliage. Perceptiveness can be used in some instances to resist a dynamic challenge effect if an opponent is, for example, attempting to hide or deceive you.

