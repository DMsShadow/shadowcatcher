# Introduction
The Tower of Kargen the Mad is a learn by play adventure for the ShadowCatcher RPG system. As well as being an introductory adventure for players, it also aims to teach new Games Masters (GMs) to run the ShadowCatcher system smoothly. This does not mean that you have to get every rule right, but instead to make sure your players enjoy playing the game and working through the adventure
Synopsis

The adventure begins with a less than auspicious welcome to the village of Belltop. Once the players gain some sort of trust with the locals, they begin to hear rumours of various strange goings in the village and the locales around it. Investigations should bring the players to the following leads.

●	The Hermit’s Hut. Though the hermit is not in any way responsible for the goings on in the town, he knows a story or two about the brigand’s camp, the haunted cave and Kargen’s tower that should help the players narrow down their investigations.

●	The Brigand’s Camp. The players are directed here by various means, but ultimately they will discover that the brigands, though a menace to the town, know nothing about what’s truly going on. However, they have some experience of the haunted cave which may help the players.

●	The Haunted Cave. This location is not the source of the strange creatures, but has become the lair of one of those that has mysteriously appeared. 

Through exploring one or more of these leads the, or perhaps by going there directly, the players will eventually come to Kargen’s Tower. This is the main location of the adventure. The tower is filled with unnatural rooms, cunning traps, cryptic puzzles and otherworldly creatures. All of these should give the players, and the GM, a chance to explore the rules of the game as well as solve the evil menacing the land.

Village terrorised by strange creatures. Rumours of the mysterious tower. PCs explore the region to seek the tower/answers.

Eventually they enter the tower and hope to seal the portal within.
## Act 1: Welcome to Belltop
In this act the players arrive at the gates of Belltop but do not get exactly the welcome they expect. If they choose to and succeed at negotiating their way in, then they are able to explore the town and find out why they are so suspicious and scared. During this scene the players should find out rumours and hooks that will point them in the direction of places to investigate.
### Scene 1: Old Bill
Read or paraphrase the following:

You have been travelling for days now and if the guide at the last village was correct, you should have reached the town of Belltop by now. Emerging from the woods you wonder just how much longer you will have to walk, when the much fabled bell-shaped hill comes into view. You give sign of relief as the farms dotted along the road confirm that you must indeed be near your destination. Just the last few miles to go.

At this point you can give the players the chance to decide if they want to go straight to the town or make any queries at the farms on the way. There is no right or wrong decision here, and if they decide to go to a farm, feel free to run the optional farm encounter in section X.

If the decide to proceed straight to Belltop, then read or paraphrase the text below:

As you walk towards the town, the hill grows in size and the farm holdings give way to the full village. You can see even from this distance that the town is divided into two main sections. A small settlement atop the bell-shaped hill surrounded by a wooden palisade, and the bulk of the town built around the base of the hill protected by similar defences. 

Drawing nearer to the town, you can also now see that some watchtowers are dotted around these walls, including one by the main gate. In fact, no sooner have you seen the tower, when you hear the tolling of a bell, and a voice begins to shout “Strangers! Brigands! Call the guards!” It looks like your welcome maybe less warm than you might have suspected.

This man is Old Bill, an old and extremely paranoid (though not without cause) member of the town militia. He is suspicious and accusing of the party when they arrive, suspecting they may be agents of the brigands that have tormented Belltop. Though he is obviously wrong, it should seem difficult to convince him.

At this point it is worth letting the players try and talk to him in character and see if they start to feel comfortable doing so. If any of them want to roll and use traits to influence Old Bill, he is a level 5 challenge (due to his paranoia) with the following Traits:

**Old Bill (CL5)**
Paranoid 	      Proud 
Stubborn 

This may very well be the first encounter the players have had and the first chance to roll any dice. As such it would be worth encouraging them to try different things and even suggesting what they might roll. Clever to persuade Bill of the logic they are not brigands, Cunning to fool him into believing they are someone they are not (or know someone important) or even Forceful to intimidate Bill into letting them past. Either way, each success should tick off one of the traits above.

Remember that if the player making the roll has a relevant Trait they can call this which will reduce the Challenge Level by one. Players can normally work together to reduce the difficulty of a challenge but feel free to skip that rule for now depending on how well your players understand the rules. That will be discussed later in the adventure.

### Reframing the Challenge
Hopefully your players will stick to trying to get past Old Bill in the way above. Though there are rules for reframing the challenge outlined in the ShadowCatcher rules, the idea of this first encounter is not to delve that deep into the rules. Therefore, if any players jump to plans such as going away and sneaking into town at night, or anything similar, it is suggested that instead you have the town guard arrive and jump straight to scene 2.
### Ending the scene
Depending on how good your players were at working around Old Bill, he will either let them through the gates, or his bell ringing will have summoned some attention. Either way, the story advances with the arrival of the town guard
### Scene 2: Guards! Guards!
This scene should begin when either the players have convinced Old Bill to let them in, or when they have decided to head away and come back later (or leave altogether). In either case, the scene begins with the town gates being opened. Read aloud or paraphrase the following:

Before the gates are even half way open you hear a gruff voice bellow “Bill, what in the name of all the gods are you ringing that bell for now!” The opening portal reveals a burly man dressed in studded armour, a short sword hanging from his belt calling up to the man in the watchtower. You quickly realise that this man is accompanied by three others attired in a similar fashion, all exuding an air of authority, or at least, self-importance.

The man shouting to Bill is Gill Mason the Reeve of the town (an old term for sheriff - which derived from Shire Reeve) and is the head of its militia. His initial exchange should be him questioning Old Bill on what he’s up to. It should also be apparent that he is exasperated at Bill’s attitude to strangers, but is still not fully trusting of the new arrivals.

This initial encounter should vary based on whether the players convinced Old Bill to let them in or not. If they successfully overcame him as a challenge, then Gill will let them in without a further challenge (albeit with some suspicions) because he knows that no one is more paranoid or stubborn than OId Bill. If they did not overcome Old Bill as a challenge, then this is an opportunity to gain entry to the town with an easier challenge.
Again, players can attempt to roleplay past Gill, but otherwise they can roll some Ability tests. Gill is a level 4 challenge (as he is not a paranoid old man) with the following Traits:

**Gill Mason (CL4)**
Suspicious 	
Has a duty to perform 

Either way, Gill will still want to ask the players some questions.
### Roleplaying Gill
Gill is the kind of lawman who wants to know everyone’s motives and so he will question the player characters on where they came from, why they are here, and anything else that may come up in the conversation he feels is relevant to know. He is blunt but not rude and is not disposed to answer many questions. 

If the players are carrying any visible weapons he will ask for these to be given up. If the PCs want to keep their weapons and/or get any answers out of him then he will only comply if the already overcame him as a challenge, otherwise they will have to convince him using the challenge card above.

If successful he will allow them to keep their weapons, but ask for them to be bonded (that is tied up so they cannot be drawn quickly. And will provide the following pieces of information if asked:

●	He will reveal the town has been suffering due to brigand raids on its mercantile shipments
●	He knows like everyone else in town that strange creatures have been encountered in the woods and farmlands (but none in the town so far)
●	He will disclose that the Mayor of the town is keen to enlist help in fighting the brigands as the militia is stretched 

### Ending the scene
### Scene 3: The Rusty Flagon
### Scene 4: The Big Man
### Scene 5 (optional): Raid!
This scene is optional if you feel it will add some excitement if your players are looking bored, or if you feel they are struggling to decide which lead to follow (or are choosing not to follow any at all). To begin the scene, read aloud or paraphrase the following:

## Act 2: Investigations around Belltop
In this act it is expected that the players are following at least one lead from the town of Belltop and are thus exploring one of the locations below. If they have not chosen to do so, then feel free to use the optional “Raid on Belltop” scene provided at the end of this act. Improvised scenes like this are always helpful to keep the action moving if your players seem a bit lost, and might nudge them in a certain direction rather that you having to force them to go somewhere.
### Scene 1: The Hermit’s Hut
### Scene 2: The Brigand’s Camp

### Scene 3: The Haunted Cave

## Act 3: The Tower
One way or another, the players should work out that the mysterious tower of Kargen the Mad is the source of the strange creatures that keep appearing near Belltop and hopefully they will seek to explore the tower and put an end to the incursions. If the players feel it is too dangerous and want to shy away from it, then see some tips later for in-game encouragement.
### Scene 1: The Dark Wood
Before the players even reach the tower, they must first navigate through the Dark Wood, a daunting prospect as centuries ago Kargen used his magic to warp the woods and making it difficult for trespassers to reach his tower.
### Scene 2: The Door
## Epilogue
By the end of the adventure...
