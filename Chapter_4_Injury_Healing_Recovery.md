## Chapter 4: Hampering, Injury and Death
Any time a character fails a test or is bested in combat, they must hamper either a trait or an ability. In ShadowCatcher, such hampering constitutes the reduced ability of the character to continue in the story. A character can continue with the story so long as they have at least one ability which isn’t hampered, but if they have all of their abilities hampered, the character is taken out for the rest of the scene, or even longer.
### Hampered Abilities
Hampered abilities usually, but not always, represent some kind of physical or psychological trauma. Essentially, the character has become injured in such a way that they cannot use the ability as well as before.

As stated previously, hampered abilities can still be used to complete tests, but the challenge's level is increased by one when that ability is being used.
### Hampered Traits
If a trait becomes hampered, usually due to being called during a test which the character failed or used to ‘soak’ the failure, then it cannot be used again until it is recovered (see later).

This hampering can be anything that makes sense as part of the narrative and can include physical injury, social exclusion, fatigue, illness etc. It is optional for characters and GMs to record what the hampering actually is, but this can add to the flavour of the game.
### Hampered Relationships
If a relationship becomes hampered as part of test or contest then it is not useable until it is recovered and could mean either damage to the relationship, injury of the individual/group or a restriction on those persons to aid the character.

Examples can include a friend being injured protecting you, a snitch being kidnapped by a criminal organisation, or a patron being displeased with the demands you have made on them.

Such hampering should reflect the likely effect on the other party if the test should fail. Recovery will involve the situation changing either by time or direct player action.
### Hampered Possessions
If a possession is hampered, it has been rendered unusable in some way, either it has become damaged, lost, powered down or otherwise removed from the immediate action.

Again, it cannot be used again until it has been recovered in some way.
### Being Taken Out
If a character has no choice but to hamper the last of their abilities as part of a test or contest, then they are taken out. A character that is taken out is removed from the scene in a way chosen by the GM and cannot return to the story until they are able to recover at least one of their abilities.

Being removed from the game in this way should not normally result in character death (see below), and such a dramatic outcome would not be ideal for the player as they have no say in what actually happens to their character.
### Conceding
If during a challenge, usually combat or other dynamic challenges, a player is concerned their character will be taken out, they can instead chose to concede the challenge. This results in them losing the challenge and being taken out. 

However, the character takes no further negative effects (i.e. no other abilities can be hampered), and the player decides how the character is removed from the current scene. They are then free to return to the next scene as they will have unhampered abilities remaining.
### Trait 'Death' and Destruction
Any permanent loss of a trait, relationship or item cannot be done without the consent of the player and should only occur from a failure during a dramatic scene or if the item is 'burned' in order to gain an automatic success. As removing such traits can reduce the strength of a character, the GM should work with the player to create a replacement very rapidly (i.e. during the same or subsequent story).
### Character Death
As stated above, being taken out shouldn’t routinely result in the death of a character. However, a player may choose to have a character die if they are taken out during a challenge. This choice must always be the players, and they then choose exactly how the character dies (overriding the Being Taken Out rule above).

The death does not have to be literal (although it’s more dramatic if it is), as they character could suffer a metaphorical death. This may be more appropriate, especially in situations where they have lost a social or intellectual conflict.

The upside to character death is that the player can create a new character with all their advantages and abilities unhampered. Albeit at the loss of their beloved character.

The new character can be at the same competence level as the old one (i.e. the same number of traits), or have only the starting traits at the GMs discretion, but this must be explained to the player before they agree to the death.
## Healing and Recovery
There are two ways in which characters can recover from their abilities and traits being hampered.
### Recovery between stories
At the end of each story, each player can chose one ability or trait to automatically unhamper. For all other traits that are hampered, the player rolls 1d20 and on an 10 or higher it recovers and becomes unhampered. Abilities are harder to recover as they represent a more substantial injury, so for these 1d20 is rolled and the ability recovered on a 15 or higher.

This recovery represents rest, recuperation, medical treatment, psychological treatment, social interaction and any other activity that can fix the damage done. This recover takes time and so does not occur during a story.
### Recovery during a story
At the GMs discretion, a player with a relevant trait (e.g. medical skill, psychoanalysis, magic etc.) can make a check to help recover one trait or ability per player either during or immediately after a scene.

To do this, the healing player must make a test with the relevant trait using a target difficulty based on the values above. On a success they recover the ability or trait. A failure results in the loss of the healing trait as normal, and potentially further hampering of the character (e.g. another ability or trait becomes hampered as well) if the GM feels the treatment being performed is risky.

Please note, that no matter which traits are used, only one test can be made per character per scene.