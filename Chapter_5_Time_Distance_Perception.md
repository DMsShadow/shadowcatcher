## Chapter 5: Time, Distance and Perception
The following sections of the rules are intended to be a helpful resource for GMs when running ShadowCatcher games.
### Time
Time in ShadowCatcher is treated more fluidly than many tactical roleplaying games. Instead of rigid one minute turns or five to six second rounds, player’s actions usually take place in scenes that make up part of a chapter or larger story (though it should be noted that turns are used when resolving dynamic challenges - see later).
#### Scene
A scene is the shortest measurement of time in ShadowCatcher, but can be anything from a few seconds to several days depending on the situation. A scene is like a scene in a film or a chapter in a book. It usually takes place in the same location or area, and involves a short interaction with the surroundings or people.
A scene may or may not involve a challenge, but at the very least the players will have to describe what their characters are doing and what they try to achieve in the scene.

Traditionally, a scene ends when the characters or the GM feels it’s time to move on to a new one. It is likely to be quite clear in practice as to when a scene begins or ends.
An example of a scene may be the characters going to the King’s audience chamber to petition him for aid in the war against the northern barbarians, or a dramatic car chase through the streets of Paris.
#### Turn
A turn is an optional unit of time that a GM will use, usually with dynamic challenges, to maintain order around the sequence of events. During a challenge a GM might move to measuring turns if they feel that players need to know when they act, when the challenge then prompts a test and so on back and forth. In ShadowCatcher there is no initiative system, instead the players always go first and determine the order of their actions themselves (see Framing the Challenge later)
#### Act
An act is another optional measure of time. It is simply a collection of scenes that make up a subsection of a story. They can be useful to divide a story into manageable sections, but they are by no means mandatory.
For example, the first act of a story might be scenes in which the characters gather clues on serious of mysterious crimes and rituals that are taking place around the city. The second act could be a series or run ins with the evil cultists who are trying to hamper their investigations. The final act could then be the scenes where the characters sneak in to the evil temple and finally face the cult leader.
#### Story
A story is a collection of scenes or acts that ultimately lead to some final or larger resolution. For example, the characters could be investigating mysterious murders in a town, and the story is resolved when either the murder is caught, or he escapes the player’s clutches. In the example of acts above, all three acts would make the story.
#### Session
A session does not relate to game time, but is the real life time period in which players get together with the GM to play through scenes or even whole stories. Some actions may only occur between sessions.
### Distance and Movement
Distance in ShadowCatcher is as abstract or as accurate as you want it to be. It is less important during scenes as to exactly how far people can move or shoot, than it is that players are able to describe what they are trying to do. If the GM feels that the goal is not achievable based on the physics of the world then then simply need to say so, if it is simply difficult they can chose to set a high challenge level if they feel it is appropriate.
Larger distances should be handled in whatever way works best with the group. If you want to use accurate maps and realistic measures of movement then feel free, but if you agree to wing it and guestimate travel times then just do that.
As with most rules in ShadowCatcher, the GM is the final arbiter in what is or isn’t allowed.
### Perception
The world is subject to many things that can affect how well your characters perceive it. Darkness, mist or smoke to name but a few. GMs can use these just for atmospheric effect, or they can be used to the level of challenges. In some settings, you may be able to obtain equipment or have species traits that can overcome these hindrances.
As a rule of thumb consider increasing the level of challenge by one if there is something effecting a player's perception during the scene. This can then be reduced back down by one level with the activation of a characters mitigating trait or piece of special equipment. It is not recommended that the difficulty level be increased by more than one or two levels in this way, but again this is at the discretion of the GM.
### Managing Scenes
This section gives advice on how to open, manage and close scenes. When a scene closes it should ideal flow into the next scene as naturally as possible.
#### The first scene
There is no hard and fast rule about who should open a scene, but often the first scene will be proposed by the GM in order to start the story. If, however, a player proposes a scene to start the story, the GM should consider it if it’s in keeping with the story so far. Taking tips from story writing, the best way to open a story is in media res, that is, in the middle of some action. 

The characters and thus the players should be thrown straight into some kind of action. Whether it be an unexpected combat, a summons in front of the King or some other similarly dramatic situation is not important, just so long as it’s engaging for the players.

It is not a great idea, for example, to open your story with the characters getting out of bed, going shopping for food, or some other mundane task. Unless of course, you plan on having some unexpected and exciting event interrupt their daily routine.
#### Managing the flow of the scene
In most cases the flow of the scene should take care of itself. The players should lead on proposing what their characters do, and GM translates this into descriptions of what happens, tests, or challenges as needed.
Occasionally a scene may start to stall. That is, the players may start to struggle with ideas of what to do next, or where they should go. In all likelihood, this is a signal that the scene is coming to an end, and the GM ought to move towards closing the scene (see below).

If there is merit in the scene continuing, then you as the GM can consider giving some hints to nudge the players in the right direction. Beware overusing this tactic however, as you can end up directing all the action and start railroading your players to where you want them to go rather than by what they want to do. 
Subtlety is better here, and maybe just have NPCs hint at information they may have, rather than suggested things out of character. Remember, NPCs have motives too. Use them to your advantage here.
#### Closing the scene
Eventually any scene will have to end. It’s more than likely that your players will start to do this for you, suggesting that they go somewhere else, or move on to questioning another character.

Ideally you should aim for this transition to be led by players as much as possible but, as stated above, sometimes the scene can begin to slow as players lose a sense of what they are doing. If you feel they have gained all they need from the scene, prompt them to consider where they can go next. 

Here again, subtlety is best. Rather than stating you can go here, here or here, perhaps ask them to summarise what they have learned, and where they think they need to go next. If all else fails, then a GM you always have the option to either suggest a next scene, or throw in some action to move things along.

The action method can sometimes be the best. Depending on your setting, a sudden attack or other dramatic occurrence (perhaps a theft or an arrest) can get things moving along nicely. Never be afraid to shake things up a bit if the plot begins to stall.
#### Proposing the next scene
Leading on from the above, at least one of the players should propose the next scene. In these cases they should describe where they are going or what they are doing to start the new scene. As GM you would then take the prompt to describe how this scene begins, and let the players begin the interaction all over again.
If you have introduced some action to start a new scene, you will have proposed the new scene yourself, but again allow the players to lead the action once it has commenced.