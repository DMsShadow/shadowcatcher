## Chapter 3: Characters
In order to play a ShadowCatcher game, you will need to create a character. In simplest terms this will represent the person you will play in the game. The information about your character will be recorded on a character sheet. In some instances the GM may have characters prepared for you in advance, but usually you will create your own.

ShadowCatcher aims to make character creation as quick as possible, but it still involves a few steps where you will need to make important decisions about who your character is.

The decisions are laid out below in eight steps that summarise character creation.
### Step1: Character Premise
It is always best to start with your initial concept before you make any decisions about the character you want to play. Do you want to play an honourable knight from a noble house, a skulking thief who is only out for themselves, or a wizard of an ancient arcane order? 

Whatever your concept, try to summarise it as a single sentence, and write this in the Character Premise line in the Facets section of the character sheet. Don’t worry about the exact wording, this can always be tweaked as the character develops, but the premise of your character should inform you of your choices in the next steps in creating your character.
### Step 2: Assign Ability Die
At this stage it is now time to think about the mechanics of your character. The outcome of many actions in the game revolve around the die type in your character’s abilities. Determining these in ShadowCatcher is simply a case of assigning a set of standard die types into each of your character’s abilities: d12, d10, d8, d6, and d4.

You should remember that as the d6 is considered the human average for an ability, that all those above the d6 show an above average talent for the approach, and that only one score (the d4), is below average.

It may help when assigning dice to think of it as prioritisation, and consider which ability you feel is most important for your character, assigning that the value of the d12, and them working down from there.

>“…the d6 is the human average for an ability…”

When thinking about where to assign each die, it is also worth looking back to your character premise. Would a knight have a high Agile ability, or would they prioritise Forcefulness over agility? Would they need to be more Cunning than Perceptive?

You will need to answer these questions yourself, as the character belongs to you, and it is therefore up to you as to how you play them.
### Step 3: Create three Traits
Now you have allocated your five ability dice, you can focus on the three things that make your character special. These should be the things that make them stand out from the crowd, and ideally be in line with your character’s premise.

All traits should start with the description “Good with/at…” Continuing the example of the Knight above, the player may choose the following traits:

* Good at fighting with a sword
* Good at riding a warhorse
* Good at Jousting with a Lance

These are appropriate and in line with the character concept.

There are no rigid guidelines on writing traits, but they should be specific but not too restrictive as to make them hard to ever call during a test or challenge.

For example, the Knights sword trait above is appropriate, but the trait “Good at fighting” may be too broad (Fighting how, or with what?). Conversely, “Good at fighting with my personal family longsword” is probably too narrow, as you wouldn’t be able to use it with any other sword. 

As a general guideline, these rules suggest a format where you have two descriptors. Further to the examples above you could have "Good at _talking_ to _Law Enforcement Officers_" or "Good with _animals_ that _live in the North Woods_". These give some specificity but don't limit you to a single item or task.

This does not mean you cannot have broader or narrower traits, but they will affect how useful or powerful characters are in the game. When considering traits, it’s worth discussing with the GM as to how specific or board he wishes you to be with these before you pick them. This will save on any disappointment. 
### Step 4: Create a Relationship
A relationship is essentially a trait that allows you to make use of a social relationship with another character in the game. These traits can only be used in situations where the named character would be able to come to your aid, and in some way help you out in the test or challenge.

The relationship should be recorded as, “Has a good relationship with…”, for example:

* Has a good relationship with the Duke of Lancaster

In the case of the above, in any situation where the player can justify either the Duke’s reputation or direct aid to be of assistance, he can call the challenge to help him in the test.

As well as individuals, players can also suggest a group to have relationships with. These may potentially be a wider field of expertise than an individual, but they tend to have less social and political impact depending on their nature.

The use of other player characters as your relationships is discouraged as they should instead work with you during the game. Also, powerful relationships (e.g. the King etc.) should not be given or taken lightly.

The GM is the final arbiter on which relationships are appropriate to take during the game.
### Step 5: Create a special Possession
Gear and equipment is handled very abstractly in ShadowCatcher. You are assumed to have access to any equipment that fits with your character premise, at the GMs discretion, but these items will not provide you with any mechanical bonus, they are simply aids to roleplaying.

Exceptions to this are special possessions that are named on your character sheet. These are items which have a powerful impact, reputation, for which you have extra special affinity, or anything else that makes them stand out from the norm.

Unlike traits above, possessions by their nature should have very specific descriptions. Carrying on our previous example, a normal longsword would give the Knight no advantage, but an “Ancestral longsword balanced perfectly for my hand” would.

Unlike the other traits, possessions like this can only be given out by the GM either as gifts from other characters, or as rewards for mission, quests etc. Again, these possessions are at the GMs discretion.
### Step 6: Create four Progressions
In opposition to your traits you should now create four progressions (sometimes referred to as progression traits) for your character. It is important to choose four as there should be one for each of your traits, excluding your special possession. The important thing to consider here is that these are not your character’s flaws, but instead they are things your character is not good at, but which they want to be better at.

Therefore, consider things that your character would want to improve, but can’t do very well at the moment. The reason why this is important will be made clear when we look at character development later.

At the GMs discretion, one of your starting progressions can be a poor relationship (see above), but it may be more appropriate to allow these to emerge through roleplaying and character development.

All progressions should start with the description “Not good with/at…” Using the example of the Knight above, the player may choose the following progressions:

* Not good at swimming in rivers
* Not good at idenitifying the heraldry of noble houses
* Not good at talking to serfs
* Not good at shooting a crossbow

These are appropriate and in line with the character concept.

The GM can call these progressions whenever he feels they are appropriate and players are free to suggest calling them at any time too. Calling a progression is a good thing, as it can earn you a destiny point and will allow you to improve your character later.
### Step 7: Create Beliefs, Motivations and a ForeShadow
This step is about considering the core belief of your character, what motivates them from just being a faceless figure in the background to a hero seeking adventure and what ties do they have to the world around them.
#### Beliefs
Your character's beliefs are best described as their core views of how the world **should** be, and in a sense, of what they want the world to be through their actions and choices. These do not have to be a belief in the spiritual sense of the world, though it can be, as it is more about a core philosophy rather than necessarily anything to do with religion.

Again, it is good to look back to your character's premise when considering what their main beliefs should be. The character sheet has space for two beliefs, but players are free to have as many or as few as they like (though we recommend at least one) if they feel it would help them play their character. Some examples of beliefs might include:

* Might makes right
* The strong should protect the weak
* It's better to be a liar and alive, rather than honest and dead
* A man's word is his bond
* The needs of the many, outweigh those of the few

These are just examples and players and GMs should work to make their own.
#### Motivations
Motivations are what drive your character to go beyond just being a face in the background. The world is populated by ordinary people, and what often distinguishes heroes from the common folk isn't how good they are, but what drives them to achieve more. 

Unlike beliefs which underlie a philosophy or world view, motivations are one word descriptors of why your character would act in a way that may put themselves in danger or go out of their way to do something that others might not. To help, some suggested examples are:

* Greed
* Honour
* Curiosity
* Desperation
* Fear
* Loyalty
* Revenge
* Justice
* Pride
* Adventure

Again these are just suggestions and players should work with GMs to create their own. The idea here is to limit them to one word, but the motivations can be elaborated fully in a character's backstory if the player wishes. You may note that not all of these are positive words, players don't have to pick motivations which are considered 'good' by the world, just ones that will make them proactive.

#### ForeShadow
Here you as the player get to work with your GM to build a destiny for your character. This should not be a firm destiny, but a vague sense of something that you may do, or not do, that could shape future events. Hence, to foreshadow them.

When considering what to write in this section it is best to consider some sort of event, person or group that you would like your character to have some future link with, then describe a vague way in which you may affect them.

It is better not to stipulate if this will be good or bad, a success or a failure, merely that your character will have a role in this. As the story develops, you are then able to steer the course of how this may or may not play out. Some example might include:

* Have a part to play in finding the Sword of Truth
* Play a role in the succession of the King
* Important to the destiny of the Tiger Tribe

As you can see, none of these explicitly state the role of the character, but they give you and the GM a point to work with in the future story.

### Step 8: Personal Touches
Now you have assigned your characters abilities, traits, relationships, progressions, and equipment you are almost finished. The character sheet has spaces to record the details that will make the character unique for you and enhance your roleplaying experience, if you have not already done so, now is a good time to look more at these.
#### Character Description
This section at the top of the front page of the character sheet is where you record all the details of what your character looks like, and a section to describe how they appear to others, including any personality traits you wish them to have.

**Pick a name.** You should come up with a suitable name for your character. There are no hard a fast rules for this, but consider something you feel fits with your concept.

**Description.** A final section is left for free text description. Here you can describe any distinctive characteristics such as tattoos, scars and the like. You can also describe clothing, general appearance and personality traits in this section.
### Step 8: Play your introduction
Now you have completed these steps you are ready to play your introduction adventure. This adventure is designed to give you a chance to get to know the game, your fellow players, and form your adventuring party.

The GM can also allow you at this time to makes some changes to your character’s traits and progressions based on your introduction. Ideally, this should represent the dramatic change in their life caused by their experience, but it can also help if you didn’t quite get your abilities right.

The aim of ShadowCatcher games is to work collaboratively with your other party members to ensure you complete goals, quests, and missions. This does not mean you have to always agree with each other, but you should work with the GM to ensure that the party is compatible enough to work together without stepping on each other’s toes.
### Character Development
Character development is done through the facing of your progressions, and the expenditure of destiny points.

As explained in chapter 1, you can earn destiny points by accepting to use a progression trait during a test, but further to this, every time you accept using a progression trait and succeed in the test, you can place a check next to the progression trait.

Once a progression has been checked, you can spend two destiny points to turn this progression into a trait. To do this, simply remove it from the progression section of your sheet, and change the “not good” section of the text to “good”.

_For example, Valar the Thief, played by Joe, was **“Not good at shadowing people”** but during an adventure he had accepted using the progression trait while he made a Cunning test on a challenge to follow a wealthy merchant back to his house._

_Despite the hindrance of the progression he still succeeded, and placed a check mark next to this progression. After the end of the adventure, Joe spent 2 of his remaining destiny points to change this progression to a trait. Now Valar has the trait **“Good at shadowing people”**._

As well as gaining the new trait, the player must then select TWO new progressions to replace it. This way the number of progressions should always match your traits.

These new progressions should be in keeping with your character’s premise or experiences during the adventure, and if possible should reflect tests he failed during the last story.
 
The GM is the final arbiter in what can and can’t be taken as a progression, but the decision should be made collaboratively and with some degree of flexibility.

_In our example, Joe has made Valar good at shadowing, but during the adventure he failed in his attempt to bluff his way past some guards. Also, Valar had a run in with the local thieves guild who are none too pleased about an unregistered thief skulking around. Therefore he adds **“Not good at bluffing people”** and **“Has a poor relationship with the Thieves Guild”** to his progressions._
#### Improved Traits
As an alternative to changing a progerssion to a trait a player can choose to 'burn' a tick on a progression to instead upgrade a Trait to an Improved Trait. Improved Traits must be changed from "Good at/with..." to "Very good at/with...".

Improved Traits are useful in they reduce the difficulty of a test by two steps instead of the usual one, but come with the risk that once they are hampered they cannot be used (whereas having a second trait might give you more stamina and flexibility).